<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 10:20 AM
 */

namespace Skipper\Pipeline;

interface Middleware
{
    /**
     * @param $passable
     * @param \Closure $next
     * @param array $args
     * @return mixed
     */
    public function handle(&$passable, \Closure $next, ...$args);
}