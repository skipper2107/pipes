<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 10:53 AM
 */

namespace Skipper\Pipeline;

class ShouldCall
{
    /** @var Middleware $middleware */
    protected $middleware;
    /** @var array $args */
    protected $args = [];

    public function __construct(Middleware $middleware, ...$args)
    {
        $this->args = $args;
        $this->middleware = $middleware;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @return Middleware
     */
    public function getMiddleware(): Middleware
    {
        return $this->middleware;
    }
}