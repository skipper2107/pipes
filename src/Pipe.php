<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 10:18 AM
 */

namespace Skipper\Pipeline;

class Pipe
{
    /** @var $traveler mixed */
    protected $traveler;

    /** @var ShouldCall[] $calls */
    protected $calls = [];

    /**
     * @param $traveler mixed
     * @return Pipe
     */
    public function send($traveler): Pipe
    {
        $this->traveler = $traveler;
        return $this;
    }

    /**
     * @param Middleware $middleware
     * @param array $args
     * @return Pipe
     */
    public function through(Middleware $middleware, ...$args): Pipe
    {
        $this->calls[] = new ShouldCall($middleware, ...$args);

        return $this;
    }

    /**
     * @param \Closure $destination
     * @return mixed
     */
    public function then(\Closure $destination)
    {
        $pipeline = array_reduce(
            array_reverse($this->calls), $this->carry(), $this->prepareDestination($destination)
        );

        return $pipeline($this->traveler);
    }

    /**
     * @return \Closure
     */
    protected function carry()
    {
        return function ($stack, ShouldCall $call) {
            return function ($passable) use ($stack, $call) {
                return $call->getMiddleware()->handle($passable, $stack, ...$call->getArgs());
            };
        };
    }

    /**
     * @param \Closure $destination
     * @return \Closure
     */
    protected function prepareDestination(\Closure $destination)
    {
        return function ($traveler) use ($destination) {
            return $destination($traveler);
        };
    }

}