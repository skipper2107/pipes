<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 11:12 AM
 */

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Skipper\Pipeline\Pipe;
use Tests\Mocks\AfterMiddlewareTest;
use Tests\Mocks\IncrementStringMiddlewareTest;
use Tests\Mocks\NullMiddlewareTest;

class PipeTest extends TestCase
{
    public function testOrder()
    {
        $pipe = new Pipe();
        $pipe->send('');
        $pipe->through(new IncrementStringMiddlewareTest)
            ->through(new IncrementStringMiddlewareTest)
            ->through(new IncrementStringMiddlewareTest)
            ->through(new IncrementStringMiddlewareTest);
        self::assertEquals('****done', $pipe->then(function ($string) {
            return $string . 'done';
        }));
    }

    public function testNull()
    {
        $pipe = new Pipe();
        $pipe->send('')
            ->through(new IncrementStringMiddlewareTest)
            ->through(new NullMiddlewareTest)
            ->through(new IncrementStringMiddlewareTest);
        self::assertNull($pipe->then(function ($string) {
            return $string;
        }));
    }

    public function testDifferentOrder()
    {
        $pipe = new Pipe;
        $pipe->send('')
            ->through(new AfterMiddlewareTest)
            ->through(new IncrementStringMiddlewareTest);
        self::assertEquals('*-deferred-', $pipe->then(function ($string) {
            return $string;
        }));
    }

    public function testArguments()
    {
        $pipe = new Pipe;
        $pipe->send('')
            ->through(new IncrementStringMiddlewareTest, 'first', 'second')
            ->through(new IncrementStringMiddlewareTest, '1', '2');
        self::assertEquals('*firstsecond*12', $pipe->then(function ($string) {
            return $string;
        }));
    }
}