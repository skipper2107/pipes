<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 11:28 AM
 */

namespace Tests\Mocks;


use Skipper\Pipeline\Middleware;

class NullMiddlewareTest implements Middleware
{

    /**
     * @param $passable
     * @param \Closure $next
     * @param array $args
     * @return mixed
     */
    public function handle(&$passable, \Closure $next, ...$args)
    {
        return null;
    }
}