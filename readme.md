# Simple middleware pipe | refined [Laravel's pipes](https://github.com/illuminate/pipeline)

```php
class AuthMiddleware implements Middleware
{
    public function handle(&$request, \Closure $next, ...$roles)
    {
        #...do some magic
    }
}

$pipe = new Pipe();
$response = $pipe->send($request)
    ->through(new AuthMiddleware, 'admin', 'super_admin')
    ->through(new SomeOtherMiddleware)
    ->then(function($request){
        #...handle the request
    });
```
